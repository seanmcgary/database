import { Sequelize, ModelOptions, Model, DataTypes, BuildOptions, ModelAttributes } from 'sequelize';
import { ModelWrapper, StaticModel, AllowedFields, FieldMessages } from '../src';

export interface UserInstanceFields {
	password: string;
	username: string;
	email: string;
}

// tslint:disable-next-line:no-any
export interface UserInstance extends UserInstanceFields, Model {
	doesPasswordMatch(storedPassword: string, providedPassword: string): boolean;
	toJSON(sanitize?: boolean): UserInstanceFields;
}

type UserInstanceStatic = StaticModel<UserInstance>;

export class User extends ModelWrapper<UserInstance, UserInstanceStatic> {
	constructor(db: Sequelize, modelName: string, attributes: ModelAttributes, options: ModelOptions) {
		super(db, modelName, attributes, options);
	}

	get allowedFields(): AllowedFields {
		return {
			create: ['email', 'password', 'username'],
			update: ['email', 'password', 'username']
		};
	}

	get fieldMessages(): FieldMessages {
		return User.fieldMessages;
	}

	static get fieldMessages(): FieldMessages {
		return {
			email: {
				invalid: 'Please provide a valid email address'
			},
			password: {
				invalid: 'Password must be at least 8 characters'
			},
			username: {
				invalid: 'Please provide a valid username',
				unique: 'That username is already taken'
			}
		};
	}
}

export default function(db: Sequelize): User {
	return new User(db, 'users', {
		email: {
			type: DataTypes.STRING,
			allowNull: true,
			defaultValue: null,
			validate: {
				isEmail: true,
				notEmpty: true
			}
		},
		username: {
			type: DataTypes.STRING,
			allowNull: false,
			validate: {
				notEmpty: true,
				len: [1, 128]
			},
			unique: true
		},
		password: {
			type: DataTypes.STRING,
			allowNull: false,
			validate: {
				len: [8, 255]
			}
		}
	}, {
		freezeTableName: true,
		timestamps: true
	});
}
