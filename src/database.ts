import {
	DatabaseError,
	ForeignKeyConstraintError,
	Model,
	Sequelize,
	SyncOptions,
	Transaction, TransactionOptions,
	UniqueConstraintError,
	ValidationError
} from 'sequelize';
import * as Promise from 'bluebird';
import * as _ from 'lodash';
import { BaseError, NotFoundError } from '@seanmcgary/errors';

import { migrate } from './migrator';
import {
	DBConfig,
	DBExtraConfig,
	DBInterface,
	FieldMessages,
	InitFn,
	KeyedObject,
	LoadModelFn,
	MigrationConfig,
	Models,
	ModelWrapper,
	SErrors, StaticModel
} from './types';

import { Gauge } from 'prom-client';
import { HookReturn } from 'sequelize/types/lib/hooks';

export interface DatabaseMetrics {
	[index: string]: Gauge;
}

export class DB implements DBInterface {
	public db: Sequelize;
	public rr: Sequelize;

	// tslint:disable-next-line:no-any
	public ready: Promise<any>;
	public migrationConfig: MigrationConfig;
	public preMigrationConfig: MigrationConfig;
	public models: Models = {};
	public rrModels: Models = {};
	public databaseMetrics: DatabaseMetrics;
	protected dbConfig: DBConfig;
	protected rrConfig: DBConfig;
	protected extraConfig: DBExtraConfig;
	// tslint:disable-next-line:no-any
	protected metricsTimer: any;

	constructor(config: DBConfig, migrationConfig: MigrationConfig, preMigrationConfig: MigrationConfig, rrConfig?: DBConfig) {
		this.migrationConfig = migrationConfig;
		this.preMigrationConfig = preMigrationConfig;
		this.extraConfig = {
			collectMetrics: true,
			..._.pick(config, ['collectMetrics'])
		};

		this.dbConfig = {
			dialect: 'postgres',
			native: false,
			logging: false,
			pool: {
				min: 3,
				max: 15
			},
			...<object>config,
		};
		this.db = new Sequelize(this.dbConfig);
		const authPromises = [this.db.authenticate()];

		if (rrConfig) {
			this.rrConfig = {
				dialect: 'postgres',
				native: false,
				logging: false,
				pool: {
					min: 2,
					max: 10
				},
				...<object>config
			};
			this.rr = new Sequelize(this.rrConfig);
			const rrPromise = this.rr.authenticate()
			.catch((err) => {
				// tslint:disable-next-line:no-console
				console.log('Read replica connection error: ', err);
				return Promise.resolve();
			});
			authPromises.push(rrPromise);
		}

		if (this.extraConfig.collectMetrics) {
			this.initializeDatabaseMetrics();
		}

		this.db.addHook('beforeBulkSync', 'preSyncMigrate', (options: SyncOptions): HookReturn => {
			// tslint:disable-next-line:no-any
			return <any>this.preSyncMigrate();
		});

		// tslint:disable-next-line:no-any
		this.db.addHook('afterBulkSync', 'afterSyncMigrate', (options: SyncOptions): HookReturn => {
			// tslint:disable-next-line:no-any
			return <any>this.migrate();
		});

		this.ready = Promise.all(authPromises)
		.then(() => this.db.sync());
	}

	public applyMigrations() {
		return this.preSyncMigrate()
		// tslint:disable-next-line:no-console
		.then(() => console.log('Syncing models'))
		.then(() => this.db.sync())
		.then(() => this.migrate());
	}

	public preSyncMigrate(): Promise<void> {
		// tslint:disable-next-line:no-console
		console.log('Running pre-sync migrations');
		return migrate(this.db, this.preMigrationConfig.path, 'SequelizePreSyncMeta');
	}

	public migrate() {
		// tslint:disable-next-line:no-console
		console.log('Running after-sync migrations');
		return migrate(this.db, this.migrationConfig.path);
	}

	public get hasReadReplica() {
		return !!this.rr;
	}

	public get rrOrDb(): Sequelize {
		if (this.rr) {
			return this.rr;
		}
		return this.db;
	}

	public get rrOrDbModels(): Models {
		if (this.rr) {
			return this.rrModels;
		}
		return this.models;
	}

	public initializeDatabaseMetrics(): DatabaseMetrics {
		this.databaseMetrics = {
			connectionPoolSize: new Gauge({
				name: 'db_connection_pool_size',
				help: 'Size of the current connection pool',
				labelNames: ['databaseName']
			}),
			connectionPoolMaxSize: new Gauge({
				name: 'db_connection_pool_max_size',
				help: 'Max size of the current connection pool',
				labelNames: ['databaseName']
			}),
			connectionsAvailable: new Gauge({
				name: 'db_connections_available',
				help: 'Number of connections currently available',
				labelNames: ['databaseName']
			})
		};
		if (this.hasReadReplica) {
			this.databaseMetrics = {
				...this.databaseMetrics,
				rrConnectionPoolSize: new Gauge({
					name: 'rr_connection_pool_size',
					help: 'Size of the current connection pool',
					labelNames: ['databaseName']
				}),
				rrConnectionPoolMaxSize: new Gauge({
					name: 'rr_connection_pool_max_size',
					help: 'Max size of the current connection pool',
					labelNames: ['databaseName']
				}),
				rrConnectionsAvailable: new Gauge({
					name: 'rr_connections_available',
					help: 'Number of connections currently available',
					labelNames: ['databaseName']
				})
			};
		}
		return this.databaseMetrics;
	}

	public reportMetrics() {
		if (this.extraConfig.collectMetrics) {
			const metrics = this.dbConnectionCount;
			this.databaseMetrics.connectionPoolSize
			.labels(this.dbConfig.database)
			.set(metrics.size);

			this.databaseMetrics.connectionPoolMaxSize
			.labels(this.dbConfig.database)
			.set(this.dbConfig.pool.max);

			this.databaseMetrics.connectionsAvailable
			.labels(this.dbConfig.database)
			.set(metrics.available);

			if (this.hasReadReplica) {
				const rrMetrics = this.rrConnectionCount;
				this.databaseMetrics.rrConnectionPoolSize
				.labels(this.dbConfig.database)
				.set(rrMetrics.size);

				this.databaseMetrics.rrConnectionPoolMaxSize
				.labels(this.dbConfig.database)
				.set(this.dbConfig.pool.max);

				this.databaseMetrics.rrConnectionsAvailable
				.labels(this.dbConfig.database)
				.set(rrMetrics.available);
			}
		}
	}

	public startMetricsPoll() {
		if (this.extraConfig.collectMetrics) {
			clearInterval(this.metricsTimer);
			this.metricsTimer = setInterval(() => {
				this.reportMetrics();
			}, 1000);
		}
	}

	public stopMetricsPoll() {
		clearInterval(this.metricsTimer);
	}

	public get dbConnectionCount() {
		const pool = _.get(this.db, 'connectionManager.pool');
		return {
			size: pool.size,
			available: pool.available
		};
	}

	public get rrConnectionCount() {
		const pool = _.get(this.rr, 'connectionManager.pool') || {};
		return {
			size: pool.size || 0,
			available: pool.available || 0
		};
	}

	public init(fn: InitFn) {
		fn(this);
	}

	// tslint:disable-next-line:no-any
	public setModel(name: string, model: ModelWrapper<Model, StaticModel<any>>): ModelWrapper<Model, StaticModel<any>> {
		this.models[name] = model;
		if (this.hasReadReplica) {
			this.rrModels[name] = model;
		}
		return model;
	}

	// tslint:disable-next-line:no-any
	public loadModel(fn: LoadModelFn): ModelWrapper<Model, StaticModel<any>> {
		return fn(this.db);
	}

	public txn(t?: Transaction) {
		if (t) {
			return { transaction: t };
		}
		return {};
	}

	public wrapTransaction(t?: Transaction, options: TransactionOptions = {}): Promise<[Transaction, boolean]> {
		if (t) {
			return Promise.all([t, false]);
		}

		return this.db.transaction(options)
		.then((txn) => Promise.all([txn, true]));
	}

	// tslint:disable-next-line:no-any max-line-length
	public wrapTransactionAndResolve<TInstance>(fn: (t: Transaction) => Promise<TInstance>, transaction?: Transaction, options: TransactionOptions = {}): Promise<TInstance> {
		return this.wrapTransaction(transaction, options)
		.spread((t: Transaction, isLocal: boolean) => {
			return fn(t)
			.then((result: TInstance) => {
				if (!isLocal) {
					return result;
				}
				return t.commit()
				.then(() => result);
			})
			.catch((err: Error | BaseError) => {
				if (isLocal) {
					t.rollback();
				}
				throw err;
			});
		});
	}

	public normalizeDbError(fieldMessages: FieldMessages = {}) {
		return (err: Error | SErrors) => {
			const isValidationError = err instanceof ValidationError;
			const isConstraintError = err instanceof UniqueConstraintError;
			const isForeignKeyConstraintError = err instanceof ForeignKeyConstraintError;
			const isDatabaseError = err instanceof DatabaseError;

			if (isValidationError || isConstraintError || isForeignKeyConstraintError || isDatabaseError) {
				const data: KeyedObject = {};

				if (isForeignKeyConstraintError) {
					err = new BaseError(err, { message: _.get(err, 'original.messageDetail') }, 400);
				} else 	if (isValidationError) {
					_.reduce(_.get(err, 'errors'), (errors, val) => {
						const key = _.camelCase(val.path);
						let message = val.message.replace(val.path, key);

						/*
							TODO - change fieldMessages to have keys that
							match the validatorName field in the sequelize
							error object
						*/
						const messageOptions = fieldMessages[key];
						if (messageOptions) {
							const { invalid, unique } = messageOptions;
							if (isConstraintError && !!unique) {
								message = unique;
							} else if (isValidationError && !!invalid) {
								message = invalid;
							}
						}

						errors[_.camelCase(key)] = message;
						return errors;
					}, data);
				}
				if (!(err instanceof BaseError)) {
					err = new BaseError(err, data, 400);
				}
			}

			throw err;
		};
	}

	public handleItemNotFound<TInstance>(customMessage: string) {
		return (item: TInstance): Promise<TInstance> => {
			if (!item) {
				throw new NotFoundError(new Error(customMessage));
			}
			return Promise.resolve(item);
		};
	}
}
