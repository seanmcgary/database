import {
	BuildOptions,
	FindOptions,
	ModelAttributes,
	ModelOptions,
	Model,
	Options as SequelizeOptions,
	Sequelize,
	Transaction,
	DataTypes, ModelAttributeColumnOptions, ValidationError, UniqueConstraintError, ForeignKeyConstraintError
} from 'sequelize';
import * as Promise from 'bluebird';
import * as _ from 'lodash';

export interface DBExtraConfig {
	collectMetrics?: boolean;
}

// tslint:disable-next-line:no-empty
export interface DBConfig extends SequelizeOptions, DBExtraConfig {}

export interface MigrationConfig {
	path: string;
}

export type StaticModel<T> = typeof Model & {
	// tslint:disable-next-line:callable-types
	new (values?: object, options?: BuildOptions): T;
};

export interface Models {
	// tslint:disable-next-line:no-any
	[key: string]: ModelWrapper<Model, StaticModel<any>>;
}

export interface DBInterface {
	db: Sequelize;
	// tslint:disable-next-line:no-any
	ready: Promise<any>;
	migrationConfig: MigrationConfig;
	models: Models;

	migrate(): void;
	// tslint:disable-next-line:no-any
	loadModel(fn: LoadModelFn): ModelWrapper<Model, StaticModel<any>>;
	init(fn: InitFn): void;
}

export interface KeyedObject {
	// tslint:disable-next-line:no-any
	[key: string]: any;
}

export interface AnyAttributes {
	// tslint:disable-next-line:no-any
	[key: string]: any;
}

export interface AllowedFields {
	create: string[];
	update: string[];
}

export interface FieldMessages {
	[key: string]: KeyedObject;
}

export type SErrors = ValidationError | UniqueConstraintError | ForeignKeyConstraintError;

// tslint:disable-next-line:no-any
export type LoadModelFn = (db: Sequelize) => ModelWrapper<Model, StaticModel<any>>;

export type InitFn = (db: DBInterface) => void;

export interface Where {
	// tslint:disable-next-line:no-any
	[key: string]: any;
}

export interface Options {
	// tslint:disable-next-line:no-any
	[key: string]: any;
}

// tslint:disable-next-line:no-any
export interface Query extends FindOptions {
	unlimited?: boolean;
}

export interface Order {
	direction: 'asc' | 'desc';
	field: string;
}

export interface Cursor {
	limit?: number;
	offset?: number;
	order?: Order;
}

export interface QueryCursor {
	limit?: number;
	offset?: number;
	order?: string[][];
	unlimited?: boolean;
}

export interface AnyInstanceFields {
	// tslint:disable-next-line:no-any
	[key: string]: any;
}

export class ModelWrapper<ModelType, StaticModelType> {
	public model: StaticModelType;

	constructor(sequelize: Sequelize, tableName: string, attributes: ModelAttributes, options: ModelOptions) {
		if (_.get(options, 'timestamps')) {
			attributes = <ModelAttributes>{
				...attributes,
				createdAt: {
					type: DataTypes.DATE,
					allowNull: true,
					defaultValue: Sequelize.fn('NOW')
				},
				updatedAt: {
					type: DataTypes.DATE,
					allowNull: true,
					defaultValue: null
				}
			};
		}
		_.set(options, 'tableName', _.snakeCase(tableName));
		// tslint:disable-next-line:no-any
		this.model = <any>sequelize.define(tableName, this.parseAttributes(attributes), options);
	}

	// default values, should be implemented by model
	get allowedFields(): AllowedFields {
		return {
			create: [],
			update: []
		};
	}

	// default value, should be implemented by model
	get fieldMessages(): FieldMessages {
		return {};
	}

	public filterFields(type: 'create' | 'update', fields: object) {
		return _.pick(fields, this.allowedFields[type]);
	}

	public get paginationDefaults() {
		return {
			limit: 25,
			offset: 0,
			order: [['id', 'desc']]
		};
	}

	public parseCursorFromOptions(options: Options = {}): FindOptions {
		const fields = <Cursor>_.defaultsDeep(options, {
			limit: 50,
			offset: 0,
			order: {
				direction: 'desc',
				field: 'created_at'
			}
		});
		const omits = ['order'];
		if (options.limit === 'all') {
			omits.push('limit', 'offset');
		}

		const otherOptions = (options.limit === 'all' ? { unlimited: true } : {});
		return {
			..._.omit(fields, omits),
			...otherOptions,
			order: [[fields.order.field, fields.order.direction]]
		};
	}

	public findAllPaginated<MT>(query: Query, cursorFields: string[] = [], t?: Transaction): Promise<[MT[], Cursor]> {
		cursorFields = _.uniq(_.flatten([
			['limit', 'offset', 'order'],
			cursorFields
		]));

		const paginationDefaults = _.omit(this.paginationDefaults, query.unlimited ? ['limit', 'offset'] : []);
		_.defaults(query, paginationDefaults);

		if (t) {
			query.transaction = t;
		}

		// tslint:disable:no-any
		return (<any>this.model).findAll(query)
		.then((results: any[]) => {
			// tslint:enable
			let cursor: Cursor = null;

			if (results.length === query.limit) {
				cursor = _.reduce(cursorFields, (c: Cursor, key: string) => {
					let val = _.get(query, key);
					if (key === 'offset') {
						val += query.limit;
					}
					if (key === 'order') {
						const [field, direction] = _.first(val);
						val = { field, direction };
					}
					_.set(c, key, val);
					return c;
				}, <Cursor>{});
			}
			return [results, cursor];
		});
	}

	protected parseAttributes(attributes: ModelAttributes): ModelAttributes {
		return _.transform(attributes, (attrs, val: ModelAttributeColumnOptions, key) => {
			if (!val.field) {
				val.field = _.snakeCase(key);
			}
			_.set(attrs, key, val);
			return attrs;
		}, {});
	}
}
