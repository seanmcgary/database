// tslint:disable
import * as Umzug from 'umzug';
import * as _ from 'lodash';
import { Sequelize } from 'sequelize';
import * as Promise from 'bluebird';
import { UmzugOptions } from 'umzug';
import { MigrationConfig } from './types';

export class CustomUmzug extends Umzug {
	// tslint:disable-next-line:no-any
	protected _findMigrations: any;

	public pending() {
		return this
		._findMigrations()
		.bind(this)
		.then(function (all: any) {
			return Promise.join(all, this.executed());
		})
		.spread((all: any, executed: any) => {
			const executedFiles = executed.map((executedMigrationFile: any) => executedMigrationFile.file.split('.')[0]);

			return all.filter((migration: any) => {
				// split off the file extension
				return executedFiles.indexOf(migration.file.split('.')[0]) === -1;
			});
		});
	}
}

export function migrate(sequelize: Sequelize, migrationPath: string, tableName: string = 'SequelizeMeta'): Promise<void> {
	const storageOptions: any = { sequelize };
	if (tableName && tableName.length) {
		storageOptions.modelName = tableName;
	}

	const options = <UmzugOptions>{
		storage: 'sequelize',
		storageOptions,
		migrations: {
			params: [sequelize, {}, () => {
				throw new Error('Migration tried to use old style "done" callback. Please upgrade to "umzug" and return a promise instead.');
			}],
			path: migrationPath,
			pattern: /\d+-\w+\.(ts|js)/
		}
	};

	const umzug = new CustomUmzug(options);

	return Promise.resolve(umzug.up())
	.then((migrations) => {
		if (migrations && migrations.length) {
			console.log(`processed ${migrations.length} new migrations`);
			_.each(migrations, (migration) => console.log(`\t${migration.file}`));
		} else {
			console.log('no new migrations to run');
		}
		return Promise.resolve();
	});
}
