.PHONY: all test

all:
	npm run build

clean:
	rm -rf dist || true

publish: clean all
	npm publish --access=public